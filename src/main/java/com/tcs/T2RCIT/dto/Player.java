package com.tcs.T2RCIT.dto;

public class Player {

	public Player() {
		// TODO Auto-generated constructor stub
	}
	private int playerId;
	private String playerName;
	private String playerdetails;
	public int getPlayerId() {
		return playerId;
	}
	public void setPlayerId(int playerId) {
		this.playerId = playerId;
	}
	public String getPlayerName() {
		return playerName;
	}
	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}
	public String getPlayerdetails() {
		return playerdetails;
	}
	public void setPlayerdetails(String playerdetails) {
		this.playerdetails = playerdetails;
	}
	public Player(int playerId, String playerName, String playerdetails) {
		this.playerId = playerId;
		this.playerName = playerName;
		this.playerdetails = playerdetails;
	}
	
	
}
