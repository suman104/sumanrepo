package com.tcs.T2RCIT.dto;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.Id;

import javax.persistence.GeneratedValue;


@Entity
public class Product {
@Id
@GeneratedValue
private Long productId;

private String ProductName;
private String subProductName;
private String productType;
private double percentOfCompletion;
private Date dateOfCompletion;


public Product(Long productId, String productName, String subProductName, String productType,
		double percentOfCompletion, Date dateOfCompletion) {
	super();
	this.productId = productId;
	ProductName = productName;
	this.subProductName = subProductName;
	this.productType = productType;
	this.percentOfCompletion = percentOfCompletion;
	this.dateOfCompletion = dateOfCompletion;
}

public Long getProductId() {
	return productId;
}
public void setProductId(Long productId) {
	this.productId = productId;
}
public String getProductName() {
	return ProductName;
}
public void setProductName(String productName) {
	ProductName = productName;
}
public String getSubProductName() {
	return subProductName;
}
public void setSubProductName(String subProductName) {
	this.subProductName = subProductName;
}
public String getProductType() {
	return productType;
}
public void setProductType(String productType) {
	this.productType = productType;
}
public double getPercentOfCompletion() {
	return percentOfCompletion;
}
public void setPercentOfCompletion(double percentOfCompletion) {
	this.percentOfCompletion = percentOfCompletion;
}
public Date getDateOfCompletion() {
	return dateOfCompletion;
}
public void setDateOfCompletion(Date dateOfCompletion) {
	this.dateOfCompletion = dateOfCompletion;
}


}
