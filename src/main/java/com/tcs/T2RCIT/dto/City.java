package com.tcs.T2RCIT.dto;

public class City {
private String city;
private String empName;
public City(String city, String empName) {
	super();
	this.city = city;
	this.empName = empName;
}
public String getCity() {
	return city;
}
public void setCity(String city) {
	this.city = city;
}
public String getEmpName() {
	return empName;
}
public void setEmpName(String empName) {
	this.empName = empName;
}

}
