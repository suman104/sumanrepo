package com.tcs.T2RCIT.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.tcs.T2RCIT.dto.Player;
@CrossOrigin(origins="http://localhost:4200")
@RestController
public class MyController {
	@GetMapping(path="/appName")
	public String getAppName() {
		return "T2RCIT1";
		//throw new RuntimeException("error has happened");
	}
	
	@GetMapping(path="/login/{username}")
	public String login(@PathVariable String username) {
		return "Hello "+username;
	}
	
	@GetMapping(path="/getUserDetails")
	public List getDetails() {
		String username="pawan";
		List list=new ArrayList<>();
		//if(username.equals("pawan")) {
			Map map=new HashMap<>();
			map.put("Name", "kusumanchi");
			map.put("city", "Hyderabad");
			list.add(map);
			
			Map map1=new HashMap<>();
			map1.put("Name", "suman");
			map1.put("city", "BBSR");
			list.add(map1);
			
			Map map2=new HashMap<>();
			map2.put("Name", "sourabh");
			map2.put("city", "Banglore");
			list.add(map2);
	    // }
		return list;
}
	@GetMapping(path="/getCityDetails")
	public List getCityDetails() {
		List list=new ArrayList<>();
		/*
		 * City city=new City("Hyderabad","Pawan"); City city1=new
		 * City("Hyderabad","Raj"); City city2=new City("Hyderabad","Rahul");
		 */

		list.add("pawan");
		list.add("Raj");
		list.add("Rahul");
		list.add("Akash");
		
		return list;
}
	
	@Autowired
	public RestTemplate restTemplate;


	


	
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/playersrating")
	public List<Player> getRatings(){
		return restTemplate.getForObject("http://playerRating/rating", ArrayList.class);
		
	}
	
	
	@RequestMapping(value="/playerdata")
	public Player getRatings(@RequestParam ("playerId") int playerId){
		return restTemplate.getForObject("http://playerRating/playerinfo?playerId="+playerId, Player.class);
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
}